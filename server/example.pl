use strict;
use warnings;

use Socket;

use Sys::Hostname;
use Time::Piece;

use Getopt::Long;

my %ARGS;

GetOptions
(    
    'socket=s' => \$ARGS{socket},
);

if(!$ARGS{socket}){
    print "
    ERROR PLEASE INPUT A SOCKET USING \"--socket\" SWITCH IN THIS FORMAT: 
    --socket IP_ADDRESS:PORT

    EX: example.pl --socket 192.168.1.1:80
    ";
    exit(1);
}

my ($ip,$port) = ($ARGS{socket} =~ /([0-9]+(?:\.[0-9]{0,3}){3}):([0-9]+)?/);

my $paddr = sockaddr_in($port, inet_aton($ip));

#-------------Create Socket Connection------------#

my $OUTPUT_SOCKET = undef;

if(!socket($OUTPUT_SOCKET,PF_INET,SOCK_STREAM,(getprotobyname('tcp'))[2])){
    die "COULD NOT CREATE SOCKET";
}

if(!connect($OUTPUT_SOCKET, $paddr)){
    die "COULD NOT CONNECT TO SOCKET WITH IP \"$ip\" USING PORT \"$port\"";
}
print "SOCKET CONNECTION ESTABLISHED\n";

#-------------------------------------------------#

# Set to autoflush buffer
$OUTPUT_SOCKET->autoflush(1);

#-------------Send Messages to Server-------------#
# Variables can be sent to the server using
# This notation: $VARIABLE_NAME={{VARIABLE_VALUE}}
#
# Any other text sent will be interpreted as plain
# text

# Send initial variables
print $OUTPUT_SOCKET 

    "\$INIT_TIME={{".localtime->datetime(T => q{ })."}}\n".
    "\$HOSTNAME={{".hostname."}}".
    "\$PACKAGE_ID={{"."TEST_PACKAGE"."}}\n".
    "\$STATUS={{danger}}\n".
    "\$PROGRESS={{0}}\n"
;

# Send plain text to server
print $OUTPUT_SOCKET 
    qq|🤔🤔 Did you 🤔🤔 ever hear 👂🖐 the Tragedy 😯 of Darth Plagueis 😌 the Wise? It's a 😱😰Sith legend😨😧. Darth Plagueis👈👈 was a 👤Dark Lord👤 of the 👹Sith👹 so 💪powerful💪 and so 👌wise👌, he could use the 🖐Force🖐 to influence the 👀midi-chlorians👀 to create😮...life😲😲. He had such a knowledge of the 🏴Dark Side🏴, he could even keep the ones he cared😍😍 about❤❤...from ⚰dying⚰. He became so 💪powerful💪, the only thing he was 😰afraid😰 of was losing his 💪power💪...which, eventually of course😮😮, he did😣. Unfortunately😯, he taught😕 his 👨apprentice👨 everything he knew. Then his apprentice 😵killed😵 him in his 😴sleep😴. Ironic. He could save others from 💀death💀...but not 😵himself😵|
;

# Increase the progress
for my $i (0 .. 10){
	print $OUTPUT_SOCKET
	    "\$PROGRESS={{".($i*10)."}}\n"
	;
	sleep(1);
}

# Send success status
print $OUTPUT_SOCKET 
	"\$STATUS={{success}}\n"
;

#-------------------------------------------------#

# Close socket connetion
close($OUTPUT_SOCKET) or die "FAILED TO CLOSE SOCKET";
print "CLOSED SOCKET\n";