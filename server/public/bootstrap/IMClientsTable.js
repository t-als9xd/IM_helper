function IMClientsTable(socket){
	this.im_clients = [];

	this.search = function(){
		var search_str = document.getElementById("search_box").value;
		var active_rows = {};
		$( "select" ).change(function() {
			$( "select option:selected" ).each(function() {
				var value = null;
				switch($( this ).context.value){
					case "1":
					value = "ID";
					break;

					case "2":
					value = "INIT_TIME";
					break;

					case "3":
					value = "PACKAGE_ID";
					break;

					case "4":
					value = "HOSTNAME";
					break;
				}

				if(value){
					for(var id = 0; id < im_clients.length;id++){
						if(im_clients[id][value].toString().toUpperCase().search(search_str.toUpperCase())!=-1){
							active_rows[id] = true;
						}else{
							if(active_rows[id]==null){
								active_rows[id] = false;
							}
							
						}
					}					
				}
			});
		}).trigger( "change" );

		for(var id in active_rows){
			var im_client_row = document.getElementById(""+id+"_im_client_row");
			if(active_rows[id]){
				if(im_clients[id]['ENABLED'] && im_client_row.style.display=="none"){
					build_row(id);
				}
			}else{
				if(im_client_row){
					hide_row(id);
				}
			}
		}
	}

	this.sort = function(el) {
		var col_index = $(el).parent().children().index($(el));
	    var switch_count = 0;
	    var table = document.getElementById("im_clients_list");
	    var switching = true;
	    var dir = "asc";
	    while (switching) {
	        switching = false;
	        var rows = table.getElementsByTagName("TR");
	        for (var i = 0; i < (rows.length - 1); i++) {
	            var should_switch = false;
	            var x = rows[i].getElementsByTagName("TD")[col_index];
	            var y = rows[i + 1].getElementsByTagName("TD")[col_index];
	            var x_value = Number(x.innerHTML);	      
	            if(!x_value){
	            	x_value = x.innerHTML.toLowerCase();
	            }
	            var y_value = Number(y.innerHTML);	      
	            if(!y_value){
	            	y_value = y.innerHTML.toLowerCase();
	            }
	            if (dir == "asc") {
	                if (x_value > y_value) {
	                    should_switch = true;
	                    break;
	                }
	            } else if (dir == "desc") {
	                if (x_value < y_value) {
	                    should_switch = true;
	                    break;
	                }
	            }
	        }
	        if (should_switch) {
	            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
	            switching = true;
	            switch_count++;
	        } else {
	            if (switch_count == 0 && dir == "asc") {
	                dir = "desc";
	                switching = true;
	            }
	        }
	    }
	}

	this.create_log_window = function(id){
		if(!document.getElementById(id+"_log_panel")){
			var new_log_panel = document.createElement("div");
			new_log_panel.classList.add('panel','panel-default');
			new_log_panel.id = id+"_log_panel";
			var new_log_panel_heading_container = document.createElement("div");
			new_log_panel_heading_container.classList.add('panel-heading');
			new_log_panel.appendChild(new_log_panel_heading_container);

			var new_log_panel_heading = document.createElement("div");
			new_log_panel_heading.classList.add("panel-title");
			new_log_panel_heading_container.appendChild(new_log_panel_heading);

			var new_log_panel_heading_title = document.createElement("h4");
			new_log_panel_heading_title.innerHTML = id+": "+im_clients[id]['PACKAGE_ID'];
			new_log_panel_heading.appendChild(new_log_panel_heading_title);

			var new_log_panel_body = document.createElement("div");
			new_log_panel_body.classList.add("panel-body","data");
			new_log_panel_body.id = id+"_log_panel_text";
			new_log_panel_body.innerHTML = im_clients[id]['TEXT'];
			new_log_panel.appendChild(new_log_panel_body);

			document.getElementById("page-wrapper").parentNode.insertBefore(new_log_panel,document.getElementById("page-wrapper").nextSibling);

			$(function(){
		        $('#'+id+"_log_panel").lobiPanel({
		        	'minHeight':500,
		        	'maxHeight':500,
		        	'maxWidth':750,
		    		'reload': false,
		    		'state': 'unpinned'
		        });
		    });
		}
	}

	this.build_row = function(id){
		var new_im_client = document.createElement("tr");
		new_im_client.id = ""+id+"_im_client_row";

		var new_im_client_buttons_container = document.createElement("td");
		new_im_client_buttons_container.style = "width:200px";
		var new_im_client_buttons_div = document.createElement("div");
		new_im_client_buttons_div.classList.add("btn-group");
		new_im_client_buttons_div.setAttribute('role',"group");
		new_im_client_buttons_div.style = "width:auto";

		var new_im_client_logs_button = document.createElement("button");
		new_im_client_logs_button.classList.add('btn','btn-default');
		new_im_client_logs_button.innerHTML = "Logs";
		new_im_client_logs_button.setAttribute('type',"button");
		new_im_client_logs_button.setAttribute('onclick',"create_log_window('"+id+"')");
		new_im_client_buttons_div.appendChild(new_im_client_logs_button);

		var new_im_client_hide_button = document.createElement("button");
		new_im_client_hide_button.classList.add('btn','btn-default');
		new_im_client_hide_button.innerHTML = "Hide";
		new_im_client_hide_button.setAttribute('type',"button");
		new_im_client_hide_button.setAttribute('onclick',"hide_row('"+id+"')");
		new_im_client_buttons_div.appendChild(new_im_client_hide_button);

		var new_im_client_delete_button = document.createElement("button");
		new_im_client_delete_button.classList.add('btn','btn-default');
		new_im_client_delete_button.innerHTML = "Delete";
		new_im_client_delete_button.setAttribute('type',"button");
		new_im_client_delete_button.setAttribute('onclick',"remove_row('"+id+"')");
		new_im_client_buttons_div.appendChild(new_im_client_delete_button);

		new_im_client_buttons_container.appendChild(new_im_client_buttons_div);

		new_im_client.appendChild(new_im_client_buttons_container);

		var new_im_client_id = document.createElement("td");
		new_im_client_id.id = ""+id+"_im_client";
		new_im_client_id.innerHTML = ""+id;
		new_im_client.appendChild(new_im_client_id);

		var new_im_client_date = document.createElement("td");
		new_im_client_date.innerHTML = im_clients[id]['INIT_TIME'];
		new_im_client.appendChild(new_im_client_date);

		var new_im_client_package_id = document.createElement("td");
		new_im_client_package_id.innerHTML = im_clients[id]['PACKAGE_ID'];
		new_im_client.appendChild(new_im_client_package_id);

		var new_im_client_hostname = document.createElement("td"); 
		new_im_client_hostname.innerHTML = im_clients[id]['HOSTNAME'];
		new_im_client.appendChild(new_im_client_hostname);

		var new_im_client_progress_container = document.createElement("td");

		var new_im_client_progress_div = document.createElement("div");
		new_im_client_progress_div.classList.add('progress','progress-striped','active');

		var new_im_client_progress_bar = document.createElement("div"); 
		new_im_client_progress_bar.id = ""+id+"_progress_bar";
		new_im_client_progress_bar.classList.add('progress-bar','progress-bar-'+im_clients[id]['STATUS']);
		new_im_client_progress_bar.classList.role = "progressbar";
		new_im_client_progress_bar.setAttribute('aria-valuenow',im_clients[id]['PROGRESS']);
		new_im_client_progress_bar.setAttribute('aria-valuemin','0');
		new_im_client_progress_bar.setAttribute('aria-valuemax','100');

		new_im_client_progress_bar.style = "width: "+im_clients[id]['PROGRESS']+"%";

		var new_im_client_progress_bar_span = document.createElement("span");
		new_im_client_progress_bar_span.classList.add('sr-only');

		new_im_client_progress_bar.appendChild(new_im_client_progress_bar_span);
		new_im_client_progress_div.appendChild(new_im_client_progress_bar);
		new_im_client.appendChild(new_im_client_progress_container);
		new_im_client_progress_container.appendChild(new_im_client_progress_div);

		if(im_clients_list.childNodes.length){
			im_clients_list.insertBefore(new_im_client,im_clients_list.childNodes[0]);
		}else{
			im_clients_list.appendChild(new_im_client);
		}         
	}

	this.update_row = function(id){
		var new_im_client_progress_bar = document.getElementById(""+id+"_progress_bar");
		new_im_client_progress_bar.classList = [];
		new_im_client_progress_bar.classList.add('progress-bar','progress-bar-'+im_clients[id]['STATUS']);
		new_im_client_progress_bar.setAttribute('aria-valuenow',im_clients[id]['PROGRESS']);
		new_im_client_progress_bar.style = "width: "+im_clients[id]['PROGRESS']+"%";

		var new_log_panel_body = document.getElementById(""+id+"_log_panel_text");
		if(new_log_panel_body){
			new_log_panel_body.innerHTML = im_clients[id]['TEXT'];
		} 
	}

	this.hide_row = function(id){
		var im_client_row = document.getElementById(""+id+"_im_client_row");
		im_client_row.style.display="none";
	}

	this.remove_row = function(id){
		socket.emit('delete_im_client',id);
	}

	this.update = function(data){
		im_clients = data;
		for(var id = 0; id < im_clients.length;id++){

			var im_client_row = document.getElementById(""+id+"_im_client_row");

			if(!im_clients[id]['ENABLED'] && im_client_row){
			   hide_row(id);
			}

			if(im_clients[id]['ENABLED'] && im_client_row){
			   update_row(id);
			}

			if(im_clients[id]['ENABLED'] && !im_client_row){
			   build_row(id);
			}
		}		
	}
	return this;
}