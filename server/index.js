
var express = require('express');
var app = express();
var http = require('http').Server(app);
var net = require('net');
var io = require('socket.io')(http);
var dns = require('dns');
var path = require('path');

app.use(express.static(path.join(__dirname, 'public/bootstrap')));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/public/bootstrap/index.html');
});

io.on('connection', function(io_socket){
});

var im_clients= [];

var id_counter = 0;

net.createServer(
	function (client_socket) {
	    var id = id_counter++;

	    im_clients[id] = {};
	    im_clients[id]['TEXT'] = "";
		im_clients[id]['ENABLED'] = true;
		im_clients[id]['ID'] = id;

	    client_socket.on('data', function (data) {
	    	var str = data.toString();
	    	var vars = str.match(/(\$(\w+)\=\{\{([\S\s]*?)\}\})/g);
	    	if(vars && vars.length){
	    		for (var i = 0; i < vars.length;i++){
	    			var var_name=vars[i].substring(vars[i].indexOf("$")+1,vars[i].indexOf("="));
	    			var var_value=vars[i].substring(vars[i].indexOf("{{")+2,vars[i].indexOf("}}"));
	    			if(var_name!='TEXT'){
						if(im_clients[id]['ENABLED']){
				    		im_clients[id][var_name] = var_value;
				    	}
	    			}   			
	    		}
	    	}
			str = str.replace(/(\$(\w+)\=\{\{([\S\s]*?)\}\}\n*\r*)/g,'');
			if(im_clients[id]['ENABLED']){
	    		im_clients[id]['TEXT'] += str.replace(/(?:\r\n|\r|\n)/g, '<br />');
			}
    		io.sockets.emit('im_clients',im_clients);
	    });

	    client_socket.on('error',function(err){
			if(im_clients[id]['ENABLED']){
	    		im_clients[id]['STATUS'] = 'error';
	    	}
	    	io.sockets.emit('im_clients',im_clients);
	    	console.log("error");
	    });

	    client_socket.on('close',function(){
	    });
	}
)
.listen(7890);

http.listen(80, function(){
  console.log('listening on *:80');
});

io.on('connection',function(socket){
	socket.emit('im_clients',im_clients);
	socket.on('delete_im_client',	function(id){
			im_clients[id]['ENABLED'] = false;
			io.sockets.emit('im_clients',im_clients);
		}
	);
});
