use strict;
use warnings;

my $NUM_TESTS;

my %ARGS;
my $JSON_ARGS;

BEGIN{	
	my $CONFIG_FILE = 'config.json';

	my @REQUIRED_ARGS = (
		'username',
		'password',

		'packages',

		'socket',

		'vm_host',
		'vm_clients',

		'vm_file_location',
		'vm_snapshot_name',
		'vm_run_location',

		'time_script_location',

		'psexec_location',
	);

	use Getopt::Long;

	GetOptions
	(
	    'help' => \$ARGS{help},
	    
	    'load' => \$ARGS{load},

	    'username=s' => \$ARGS{username},
	    'password=s' => \$ARGS{password},

	    'packages=s' => \$ARGS{packages},

	    'socket=s' => \$ARGS{socket},

	    'vm_host=s' => \$ARGS{vm_host},
	    'vm_clients=s' => \$ARGS{vm_clients},

	    'vm_file_location=s' => \$ARGS{vm_file_location},
	    'vm_snapshot_name=s' => \$ARGS{vm_snapshot_name},
	    'vm_run_location=s' => \$ARGS{vm_run_location},

	    'psexec_location=s' => \$ARGS{psexec_location},

	    'time_script_location=s' => \$ARGS{time_script_location},
	);

	if($ARGS{help}){
		print
qq|
usage: perl run.pl [--help] [--load] [--username u] [--password p] [--packages i] [--socket t] [--vm_host h] [--vm_clients c] [--vm_file_location l] [--vm_snapshot_name s] [--vm_run_location f] [--psexec_location z] [--time_script_location x]  

	--load 		                  : Load JSON config file

    --packages i                  : Comma seperated string of package ids (Must be in itwindist\\win10 directory)

    --socket t                    : IP and port of server to handle logging

	--vm_host h                   : IP address of VM host pc 
    --vm_clients c                : Comma seperated string of VM client's possible IP addresses

    --vm_file_location l          : Directory of the vm_clients c .vmx file
    --vm_snapshot_name s          : Name of the snapshot to restore to after each package attempt
    --vm_run_location f           : Location of vm_run.exe located on vm_host h

    --psexec_location z           : Location of Psexec executable (used by pc calling tester script)

    --time_script_location        : Location of script to update time after vm_clients are restored to a snapshot

example: perl run.pl --load "C:\\config.json" --username mst-users\\t-als9xd --password SOME_PASSWORD --packages PACKAGE.2017,OTHER_PACKAGE.2018 --vm_clients 192.168.1.1,192.168.1.2 --vm_host 192.168.1.3 --socket 192.168.1.1:8080

note: 		
	Arguments passed through the command line will overwrite those in config file.

	Creditials u,p required for 
		1. mounting \\\\minerfiles.mst.edu\\dfs network drive
		2. Running command to update time on virtual machine 

IMPORTANT:
	As of now an open RDP session to vm_host h is required
|;
		exit(0);
	}

	if($ARGS{vm_clients}){
		my @split = split(',',$ARGS{vm_clients});
		$ARGS{vm_clients} = \@split;
	}
	if($ARGS{socket}){
		my @split = split(',',$ARGS{socket});
		$ARGS{socket} = \@split;
	}
	if($ARGS{packages}){
		my @split = split(',',$ARGS{packages});
		$ARGS{packages} = \@split;
	}

	use JSON "decode_json";

	local $/ = undef;
	open FILE, $CONFIG_FILE or die "Couldn't open config file: $!";
	binmode FILE;
	my $json_text = <FILE>;
	close FILE;

	$JSON_ARGS = decode_json($json_text);
	foreach my $arg (@REQUIRED_ARGS){
		if(!(defined $ARGS{$arg})){
			if($JSON_ARGS->{$arg}){
				$ARGS{$arg} = $JSON_ARGS->{$arg};
			}else{
				die("ERROR: ARG \"$arg\" REQUIRED");
			}

		}
	}
	$NUM_TESTS = scalar @{$ARGS{packages}};
}

use Test::More tests=> $NUM_TESTS;

print "LOOKING FOR OPEN RDP SESSIONS\n";
my $rdp_string = `\"$ARGS{psexec_location}\" -s \\\\$ARGS{vm_host} powershell \"\$session = tasklist /fo CSV | findstr RDP ; \$session = \$session.Split(\\\",\\\")[3] ; \$session.Split('\\\"')[1]\"`;
my @rdp_pid = ($rdp_string =~ /\n(\d+)\n/);
if(!(scalar @rdp_pid) || ($rdp_pid[0] eq "")){
	die("FAILED\n");
}
print "FOUND RDP SESSION WITH ID \"$rdp_pid[0]\"\n\n";

foreach my $package (@{$ARGS{packages}}){

	print "REVERTING VIRTUAL MACHINE TO SNAPSHOT \"$ARGS{vm_snapshot_name}\"\n";
	`\"$ARGS{psexec_location}\" -s -i $rdp_pid[0] \\\\$ARGS{vm_host} cmd /C \"\"$ARGS{vm_run_location}\" revertToSnapshot \"$ARGS{vm_file_location}\" \"$ARGS{vm_snapshot_name}\"\"`;
	!$? or die "FAILED";
	print "DONE\n\n";

	print "STARTING VIRTUAL MACHINE\n";
	`\"$ARGS{psexec_location}\" -s -i $rdp_pid[0] \\\\$ARGS{vm_host} cmd /C \"\"$ARGS{vm_run_location}\" start \"$ARGS{vm_file_location}\"\"`;
	!$? or die "FAILED";
	print "DONE\n\n";

	print "SETTING TIME IN VIRTUAL MACHINE USING \"$ARGS{time_script_location}\"\n";
	do{
		`\"$ARGS{psexec_location}\" -s -i $rdp_pid[0] \\\\$ARGS{vm_host} cmd /C \"\"$ARGS{vm_run_location}\" -gu $ARGS{username} -gp $ARGS{password} runProgramInGuest \"$ARGS{vm_file_location}\" -activeWindow -interactive \"$ARGS{time_script_location}\"\"`;
	}
	while(!($? >> 8 == 2));
	print "DONE\n\n";

	foreach my $vm_client (@{$ARGS{vm_clients}}){
		print "LOOKING FOR VIRTUAL MACHINE AT \"$vm_client\"\n";

		my $max_retries = 10;
		my $retries = 0;
		do{
			print "WAITING A BIT FOR ANY IP CHANGES\n";
			sleep(30);
			print "DONE\n\n";
			`ping -w 2999 $vm_client`;
			$retries++;
		}while ($? && ($retries <= $max_retries));
		
		if (!$?) {
			print "FOUND VIRTUAL MACHINE AT \"$vm_client\"\n\n";
			
			print "ADDING NETWORK DRIVE\n";
			`\"$ARGS{psexec_location}\" -s -i -u $ARGS{username} -p $ARGS{password} \\\\$vm_client cmd /C \"net use O: \\\\minerfiles.mst.edu\\dfs $ARGS{password} /USER:$ARGS{username}\"`;
			!$? or die "FAILED";
			print "DONE\n\n";

			print "INSTALLING \"$package\"\n";
			`\"$ARGS{psexec_location}\" -s -i -u $ARGS{username} -p $ARGS{password} \\\\$vm_client cmd /C \"O: && cd O:\\software\\itwindist\\win10\\appdist\\$package && perl update.pl --socket $ARGS{socket}\"`;
			is($?,0);

		}else{
			print "COULD NOT FIND VIRTUAL MACHINE AT \"$vm_client\"\n";
		}
	}

}

exit(0);