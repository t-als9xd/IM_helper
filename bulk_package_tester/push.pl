use strict;
use warnings;

use File::Copy;
use Getopt::Long;
use JSON "decode_json";

my %ARGS;

my $CONFIG_FILE = 'config.json';

my @REQUIRED_ARGS = (
	'packages',
	'im_helper_location'
);

GetOptions
(
    'help' => \$ARGS{help},
    'packages=s' => \$ARGS{packages},
    'im_helper_location=s' => \$ARGS{im_helper_location}
);

if($ARGS{help}){
	print
qq|
description: pushes a local copy of IM_helper.pm to remote directories 

usage: perl push.pl [--help] [--packages i] [--im_helper_location h]

    --packages i                  : Comma seperated string of package ids (Must be in itwindist\\win10 directory)

    --im_helper_location h        : location of IM_helper.pm

example: perl push.pl --packages EXAMPLE.2017\\prod,OTHER_EXAMPLE.2018\\dev

note: 		
	Arguments passed through the command line will overwrite those in config file.

|;
	exit(0);
}

if($ARGS{packages}){
	my @split = split(',',$ARGS{packages});
	$ARGS{packages} = \@split;
}

local $/ = undef;
if(!open FILE, $CONFIG_FILE){
	print "COULDN'T OPEN CONFIG FILE AT \"$CONFIG_FILE\": $!";
}else{
	binmode FILE;
	my $json_text = <FILE>;
	close FILE;

	my $JSON_ARGS = decode_json($json_text);
	foreach my $arg (@REQUIRED_ARGS){
		if(!(defined $ARGS{$arg})){
			if($JSON_ARGS->{$arg}){
				$ARGS{$arg} = $JSON_ARGS->{$arg};
			}else{
				die("ERROR: ARG \"$arg\" REQUIRED");
			}

		}
	}	
} 

foreach my $package (@{$ARGS{packages}}){
	my $package_path = "\\\\minerfiles.mst.edu\\dfs\\software\\itwindist\\win10\\appdist\\$package";
	if(!(-e $package_path) || !(-d $package_path)){
		print "INVALID DIRECTORY \"$package_path\"\n";
	}else{
		print "COPYING \"$ARGS{im_helper_location}\" TO \"$package_path\"\n";
		if(copy(
			$ARGS{im_helper_location},
			$package_path
		)){
			print "SUCCESS\n\n";
		}else{
			die "FAILED\n";
		}		
	}
}
exit(0);
