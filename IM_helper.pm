#!/usr/bin/perl -X

# Git Repo:
# https://git.mst.edu/t-als9xd/IM_helper

package IM_helper;

use strict;
use warnings;

############################

use base qw(Exporter);

our @ISA = qw(Exporter);
our @EXPORT = qw();

our $VERSION = v4.04;

use Win32::Console;

my $CONSOLE = Win32::Console->new();
Win32::Console::OutputCP( 65001 );

############################

my $INSTALLMONKEY_OPTIONS;

use lib ( '\\\\minerfiles.mst.edu\dfs\software\loginscripts\im',
    $ENV{'WINDIR'} . '\SYSTEM32\UMRINST', 'C:\temp' );

use InstallMonkey::Shared;

############################

use Time::Piece;

use Win32::TieRegistry(Delimiter=>"/");

use Archive::Extract;

use File::Copy;
use File::Path;
use File::Basename;

use Sys::Hostname;

use Data::Dumper;

############################

our %call_stack_set;

our @OUTPUT_STREAM;
our %OUTPUT_TRANSFORMATIONS;

sub catch_success;
sub catch_error;

############################

our %IM_HELPER_OPTIONS = {};

$IM_HELPER_OPTIONS{help} = 0;
our $HELP_MESSAGE = qq|
usage: perl update.pl [--help] [--quiet] [--list] [--load f] [--override c] [--socket t]  
    --list         : list all loaded call stacks ids
    --load f       : filename of call stack set file to be loaded (overrides default) 
    --override c   : comma seperated string of call stack ids (in order of execution)
    --socket t     : ip and port of server listening on for socket connections

example: perl update.pl --load custom.pl --override first,then,finally --socket 192.168.1.1:8080
|;

$IM_HELPER_OPTIONS{call_stack_override} = ();
$IM_HELPER_OPTIONS{load_override} = undef;
$IM_HELPER_OPTIONS{socket_server} = undef;
$IM_HELPER_OPTIONS{list} = 0;
$IM_HELPER_OPTIONS{quiet} = 0;

our $source_files;

use Getopt::Long;
Getopt::Long::Configure(qw/pass_through/);
GetOptions
(
    'help' => \$IM_HELPER_OPTIONS{help},
    'override=s' => \$IM_HELPER_OPTIONS{call_stack_override},
    'load=s' => \$IM_HELPER_OPTIONS{load_override},
    'socket=s' => \$IM_HELPER_OPTIONS{socket_server},
    'list' => \$IM_HELPER_OPTIONS{list},
    'quiet' => \$IM_HELPER_OPTIONS{quiet}
);

$IM_HELPER_OPTIONS{call_stack_override} = [split(/,/,join(',',$IM_HELPER_OPTIONS{call_stack_override}))];

our $exec_call_stack_override = undef;
if(scalar @{$IM_helper::IM_HELPER_OPTIONS{call_stack_override}}){
    $exec_call_stack_override = sub {
        foreach my $call_stack (@{$IM_helper::IM_HELPER_OPTIONS{call_stack_override}}){
            if(!IM_helper::exec_call_stacks(id=>$call_stack)){
               return 0;
            }
        }
        return 1;
    }
}

if($IM_HELPER_OPTIONS{help}){
    flush_msg(
        msg=> $HELP_MESSAGE
    );
    IM_Exit(EXIT_FAILURE);
}

sub import {

    shift;
    
    $INSTALLMONKEY_OPTIONS = shift;
    $source_files = "C:\\SourceFiles\\$INSTALLMONKEY_OPTIONS->{package_id}";
    
    if(!((my $requested_version = shift) eq $VERSION)){
        flush_msg(
            msg=>"ATTEMPTED TO IMPORT IM_HELPER VERSION \"".sprintf("%vd",$requested_version)."\" BUT GOT \"".sprintf("%vd",$VERSION)."\"",
            type=>'error'
        );
        IM_Exit(EXIT_FAILURE);        
    }

    if(!(@_) && $@){
        flush_msg(
            msg=>"COULD NOT COMPILE CALL STACK SET FILE:\n$@\"",
            type=>'error'
        );
        if($!){
            flush_msg(
                msg=>"COULD NOT \"DO\" CALL STACK SET FILE:\n$!",
                type=>'error'
            );
        }
        IM_Exit(EXIT_FAILURE);
    }
    my %new_call_stack_set = @_;
    if(!$IM_HELPER_OPTIONS{load_override}){
        if(%new_call_stack_set){
            %call_stack_set = (%call_stack_set, %new_call_stack_set);
            if ($IM_HELPER_OPTIONS{list}){
                foreach my $call_stack (keys %call_stack_set){
                    flush_msg(msg=>"- $call_stack\n");
                } 
                IM_Exit(EXIT_FAILURE);
            }
            if(scalar @{$IM_HELPER_OPTIONS{call_stack_override}}){
                foreach my $call_stack (@{$IM_HELPER_OPTIONS{call_stack_override}}){
                    if(!(exists $call_stack_set{$call_stack})){
                        flush_msg(
                            msg=>"COULD NOT FIND CALL STACK \"$call_stack\"",
                            type=>'error'
                        );
                        my $closest_match = undef;
                        my $closest_score = undef;
                        foreach my $valid_call_stack (keys %call_stack_set){
                            my $score = levenshtein_distance($call_stack,$valid_call_stack);
                            if(!defined $closest_score || ($score < $closest_score)){
                                $closest_score = $score;
                                $closest_match = $valid_call_stack;
                            }
                        }
                        if($closest_match && $closest_score<=(length($closest_match)/2)){
                            flush_msg(msg=>":: DID YOU MEAN TO RUN \"$closest_match\"?\n");
                        }
                        flush_msg(msg=>"\n");
                        IM_Exit(EXIT_FAILURE);
                    }
                }
            }            
        }
    }else{
        if(!(-e $IM_HELPER_OPTIONS{load_override})){
            flush_msg(
                msg=>"COULD NOT FIND CALL STACK SET FILE AT \"$IM_HELPER_OPTIONS{load_override}\"",
                type=>'error'
            );
            IM_Exit(EXIT_FAILURE);
        }
        if(-d $IM_HELPER_OPTIONS{load_override}){
            flush_msg(
                msg=>"\"$IM_HELPER_OPTIONS{load_override}\" IS A DIRECTORY",
                type=>'error'
            );
            IM_Exit(EXIT_FAILURE);
        }
        %call_stack_set = (%call_stack_set, do $IM_HELPER_OPTIONS{load_override});
    }

    if(
        $IM_HELPER_OPTIONS{socket_server} 
        &&
        !init_socket_conn(
            succ_cb => \&IM_helper::catch_success,
            fail_cb => \&IM_helper::catch_error
        )){
        IM_Exit(EXIT_FAILURE);
    }

}

#####################################################################################################################################
#--------------------------------------------------------Generic Functions----------------------------------------------------------#
#####################################################################################################################################

#TODO: parse args to this function
sub parse_args{
    
    my %args = @_;


    if(exists $args{optional_args} && exists $args{passed_args}){
        my $arg_err = 0;

        my %required_args = map {$_ => 1 } @{$args{required_args}};
        my %optional_args = map {$_ => 1 } @{$args{optional_args}};

        my %all_args = (%required_args,%optional_args);

        foreach my $passed_arg ( keys %{$args{passed_args}} ) {
            if(!(exists $all_args{$passed_arg})){
                flush_msg(
                    msg=>"EXTRANEOUS ARGUMENT \"$passed_arg\" SUPPLIED TO FUNCTION $args{function_name}",
                    type=>'error'
                    );
                    my $closest_match = undef;
                    my $closest_score = undef;
                    foreach my $valid_arg (keys %all_args){
                        my $score = levenshtein_distance($passed_arg,$valid_arg);
                        if(!defined $closest_score || ($score < $closest_score)){
                            $closest_score = $score;
                            $closest_match = $valid_arg;
                        }
                    }
                    if($closest_match && $closest_score<=(length($closest_match)/2)){
                        flush_msg(msg=>":: DID YOU MEAN TO PASS \"$closest_match\"?\n");
                    }
                    flush_msg(msg=>"\n");
                $arg_err++;
            }
        }

        foreach my $required_arg ( @{$args{required_args}} ) {
            if(!(exists $args{passed_args}->{$required_arg}) ){
                flush_msg(
                    msg=>"$required_arg NOT SUPPLIED TO FUNCTION $args{function_name}",
                    type=>'error'
                );
                $arg_err++;
            }
        }
        if($arg_err){
            flush_msg(msg=>"\n- USAGE -\n$args{usage}\n\n");
            return undef;
        }

        if(exists $args{passed_args}->{quiet} && $args{passed_args}->{quiet}){
            return $args{passed_args};
        }

        if(!$args{passed_args}->{fail_cb}){
            $args{passed_args}->{fail_cb} = \&catch_error;
        }else{
            my $old_cb = $args{passed_args}->{fail_cb};
            if(!($old_cb eq \&catch_error)){
                $args{passed_args}->{fail_cb} = sub {catch_error(@_); return $old_cb->(@_);};
            }
        }
        if(!$args{passed_args}->{succ_cb}){
            $args{passed_args}->{succ_cb} = \&catch_success;
        }else{
            my $old_cb = $args{passed_args}->{succ_cb};
            if(!($old_cb eq \&catch_success)){
                $args{passed_args}->{succ_cb} = sub {catch_success(@_); return $old_cb->(@_);};
            }
        }
        
        return $args{passed_args};

    }

    return undef;
}

use List::Util qw(min);

sub levenshtein_distance{

    my $eq_with_diacritics = sub {
        my ($x, $y) = @_;
        return $x eq $y;
    };

    my $eq_without_diacritics;

    my $opt = pop(@_) if @_ > 0 && ref($_[-1]) eq 'HASH';
    flush_msg(msg=>"fastdistance() takes 2 or 3 arguments",type=>'error') unless @_ == 2;
    my ($s, $t) = @_;
    $s = lc($s);
    $t = lc($t);
    my (@v0, @v1);
    my ($i, $j);
    my $eq;

    $opt = {} if not defined $opt;
    if ($opt->{ignore_diacritics}) {
        if (not defined $eq_without_diacritics) {
            require Unicode::Collate;
            my $collator = Unicode::Collate->new(normalization => undef, level => 1);
            $eq_without_diacritics = sub {
                return $collator->eq(@_);
            };
        }
        $eq = $eq_without_diacritics;
    }
    else {
        $eq = $eq_with_diacritics;
    }

    return 0 if $s eq $t;
    return length($s) if !$t || length($t) == 0;
    return length($t) if !$s || length($s) == 0;

    my $s_length = length($s);
    my $t_length = length($t);

    for ($i = 0; $i < $t_length + 1; $i++) {
        $v0[$i] = $i;
    }

    for ($i = 0; $i < $s_length; $i++) {
        $v1[0] = $i + 1;

        for ($j = 0; $j < $t_length; $j++) {
            my $cost = $eq->(substr($s, $i, 1), substr($t, $j, 1)) ? 0 : 1;
            $v1[$j + 1] = List::Util::min(
                              $v1[$j] + 1,
                              $v0[$j + 1] + 1,
                              $v0[$j] + $cost,
                             );
        }

        for ($j = 0; $j < $t_length + 1; $j++) {
            $v0[$j] = $v1[$j];
        }
    }

    return $v1[ $t_length];
}

sub normalize_text{
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["text"],
        optional_args => ["quiet"],
        usage => 
qq|
EX: normalize_text(
        text=>q\|
                This is some example text
                More example text
        \|
    );|
    );

    if(!$args){
       return 0;
    }   

    #Remove leading newline in string
    $args->{text} =~ s/^\s+|\s+$//g;
    #Remove leading whitespace in each line
    $args->{text} =~ s/^[ \t]*//mg;
    return ($args->{text});
}

#####################################################################################################################################
#-----------------------------------------Functions for Manipulating Call Stacks----------------------------------------------------#
#####################################################################################################################################

sub override_exists{
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["id"],
        optional_args => ["quiet","succ_cb","fail_cb"],
        usage => 
qq|
EX: override_exists(
        id=>'my_call_stack'
    );|
    );

    if(!$args){
       return 0;
    }
    my %overrides = map { $_ => 1 } @{$IM_HELPER_OPTIONS{call_stack_override}};

    if(exists $overrides{$args->{id}}){
        my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "FOUND OVERRIDE \"$args->{id}\"");
        return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);
    }else{
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT FIND OVERRIDE \"$args->{id}\"");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }
}

sub exec_call_stacks{
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => [],
        optional_args => ["id","ids","succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: exec_call_stacks(
        id=>'my_call_stack',
        ids=>['my_call_stack','my_other_call_stack']
    );|
    );

    if(!$args){
       return 0;
    }
    if(!(!(exists $args->{id}) != !(exists $args->{ids}))){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "EXACTLY ONE OF ARGUMENTS \"id\" OR \"ids\" IS REQUIRED IN FUNCTION ".(caller(0))[3]);
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }
    
    my @ids = (exists $args->{id}) ? ($args->{id}): @{$args->{ids}};

    my $num_subs = 0;
    foreach my $id (@ids){
    	if(exists $call_stack_set{$id}){
    		$num_subs+=scalar @{$call_stack_set{$id}};
    	}
    }

    my $progress = 0;
    socket_send(
    	"\$PROGRESS={{$progress}}".
    	"\$STATUS={{warning}}"
    );
    
    foreach my $id (@ids){
	    if(!(exists $call_stack_set{$id})){
	        flush_msg(
	            msg=>"CALL STACK \"$id\" DOESN'T EXIST",
	            type=>'error'
	        );
	        my $closest_match = undef;
	        my $closest_score = undef;
	        foreach my $valid_call_stack (keys %call_stack_set){
	            my $score = levenshtein_distance($id,$valid_call_stack);
	            if(!defined $closest_score || ($score < $closest_score)){
	                $closest_score = $score;
	                $closest_match = $valid_call_stack;
	            }
	        }
	        if($closest_match && $closest_score<=(length($closest_match)/2)){
	            flush_msg(msg=>":: DID YOU MEAN TO RUN \"$closest_match\"?\n");
	        }
	        flush_msg(msg=>"\n");
	        IM_Exit(EXIT_FAILURE);
	    }

	    flush_msg(msg=>"\n");
	    flush_msg(msg=>"RUNNING \"$id\" CALL STACK",type=>"title");
	    flush_msg(msg=>"\n");

	    foreach my $call_stack (@{$call_stack_set{$id}}) {
	    	$progress+=(100/$num_subs);
	    	socket_send("\$PROGRESS={{$progress}}");
	        if(!$call_stack->()){
	        	socket_send("\$STATUS={{danger}}");
		        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "CALL STACK \"$id\" FAILED");        
	        	return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
	    	}
	    }
	    flush_msg(msg=>"FINISHED \"$id\" CALL STACK\n",type=>"title");
    }
	socket_send("\$STATUS={{success}}");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>undef):1); 
}

#####################################################################################################################################
#-----------------------------------------Function for running a command------------------------------------------------------------#
#####################################################################################################################################

use Win32::Process qw(STILL_ACTIVE NORMAL_PRIORITY_CLASS INFINITE);

sub run_command{
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["command"],
        optional_args => ["description","input","input_file","ignore_codes","timeout","show_output","succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: run_command(
        description=>'example description',
        command=>'example_command.exe',
        ignore_codes=>[1,87],
        timeout=>10
        show_output=>1,
        input=>"hello program",
        input_file=>"example_input.txt"
    );
    
Note: - fail_cb is implicitly passed exit_code
      - timeout is in seconds|
    );

    if(!$args){
       return 0;
    }

    my $cmd = $args->{command};
    
    if(exists $args->{description}){
        flush_msg(msg=>"\"$args->{description}\"",type=>"running");        
    }

    if(!(exists $args->{quiet} && $args->{quiet})){    
        flush_msg(msg=>$cmd,type=>"box_light");
    }
    local ( *_STDIN, *_STDOUT, *_STDERR, *RE_IN, *RE_OUT, *RE_ERR );

    # Save the default streams.
    open( *_STDIN,  "<&STDIN" );
    open( *_STDOUT, ">&STDOUT" );
    open( *_STDERR, ">&STDERR" );

    # Generate temp files for redirected streams.
    my $input_file  = gen_temp_file_name("cmd_in");
    my $stdout_file = gen_temp_file_name("cmd_out");
    my $stderr_file = gen_temp_file_name("cmd_err");

    my ( $close_output, $close_error );
    $close_output = $close_error = 0;

    # If the user specified any input, create an input stream with that
    #   input. Otherwise, use an empty file as the input stream.
    my $stdin_fh          = \*STDIN;
    my $input             = undef;
    my $delete_input_file = 1;
    if ( exists( $args->{input_file} ) ) {
        $delete_input_file = 0;    # Don't delete it if we don't create it.
        $input_file = $args->{input_file};
        if ( !open( STDIN, "<", $input_file ) ) {
            if(!(exists $args->{quiet} && $args->{quiet})){
                flush_msg(
                    msg=>"CANNOT REDIRECT STDIN \"${input_file}\" FOR SYSTEM CALL: $!",
                    type=>'error'
                );
            }
        }
    }
    elsif ( exists( $args->{input} ) ) {
        $input = $args->{input};
        if ( !open( RE_IN, ">", $input_file ) ) {
            if(!(exists $args->{quiet} && $args->{quiet})){
                flush_msg(
                    msg=>"CANNOT CREATE \"${input_file}\" FOR SYSTEM CALL: $!",
                    type=>'error'
                );
            }
        }
        else {
            print RE_IN $input;
            close(RE_IN);
            if ( !open( STDIN, "<", $input_file ) ) {
                if(!(exists $args->{quiet} && $args->{quiet})){
                    flush_msg(
                        msg=>"CANNOT REDIRECT POPULATED STDIN FOR SYSTEM CALL: $!",
                        type=>'error'
                    );
                }
            }
        }
    }
    else {
        $input_file = 0;
    }

    # Send the known command info to the log before we run the command.
    # If the command hangs or takes a long time, we at least want the
    #   output log to have info about what it's doing.
    if(!(exists $args->{quiet} && $args->{quiet})){
       output(
        "Command info: $args->{description}\n" 
            . "  Command: ${cmd}\n"
            . (
            defined($input)
            ? "  Standard Input:\n\t"
                . join( "\n\t", split( /\n(?:\r?)/, $input ) ) . "\n"
            : ""
            ),
        'only' => 'logfile'
        );     
    }


    if ( open( RE_OUT, ">", $stdout_file ) ) {
        open( STDOUT, '>&RE_OUT' );
        $close_output = 1;
    }
    else {
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT OPEN STDOUT REDIRECTION FILE \"$stdout_file\" FOR READING:\n$!");
        return exists $args->{fail_cb}? $args->{fail_cb}->(
            err=> $err,
            cmd=>$cmd, 
            description=>$args->{description},  
        ):0;
    }

    if ( open( RE_ERR, ">", $stderr_file ) ) {
        open( STDERR, '>&RE_ERR' );
        $close_error = 1;
    }
    else {
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT OPEN STDERR REDIRECTION FILE \"$stdout_file\" FOR READING:\n$!");
        return exists $args->{fail_cb}? $args->{fail_cb}->(
            err=> $err,
            cmd=>$cmd, 
            description=>$args->{description},  
        ):0;
    }

    my $premature_exit = 0;

    my $start_time = time();
    Win32::Process::Create(my $cmd_obj, "C:\\Windows\\System32\\cmd.exe","cmd /C \"$cmd\"",0,NORMAL_PRIORITY_CLASS,".");        
    
    my $read_out_fh = undef;
    if(exists $args->{show_output} && $args->{show_output}){
        open($read_out_fh, "<", $stdout_file );
    }
    my $rc = undef;

    my $counter = 0;
    while(!(defined $rc) || $rc == STILL_ACTIVE){
        sleep 1;
        if(exists $args->{timeout} && $args->{timeout}){
            if(++$counter >= $args->{timeout}){
                $premature_exit = 1;
                $cmd_obj->Kill(1);
            }
        }
        $cmd_obj->GetExitCode($rc);
        if(exists $args->{show_output} && $args->{show_output} && defined $read_out_fh){
            while (<$read_out_fh>){
                open( STDOUT, '>&_STDOUT' );
                chomp $_;
                flush_msg(msg=>">> $_\n");
                open( *_STDOUT, ">&STDOUT" );
            }
            seek $read_out_fh, 0, 1; 
        }
        if(!(exists $args->{show_output} && $args->{show_output}) && !(exists $args->{quiet} && $args->{quiet})){
            my $s = time() - $start_time;
            open( STDOUT, '>&_STDOUT' );
            print(sprintf("\r: - Elapsed Time: %02d:%02d:%02d", int($s/3600), int($s/60)%60, $s%60));
            open( *_STDOUT, ">&STDOUT" );
        }
    } 

    my $s = time() - $start_time;
    if(!(exists $args->{show_output} && $args->{show_output}) && !(exists $args->{quiet} && $args->{quiet})){
        flush_msg(
            msg=> sprintf("\r: - Elapsed Time: %02d:%02d:%02d", int($s/3600), int($s/60)%60, $s%60)
        );     
    }

    if(exists $args->{show_output} && $args->{show_output}){
        close($read_out_fh);
    }
    # Restore the default streams.
    open( STDIN,  '<&_STDIN' );
    open( STDOUT, '>&_STDOUT' );
    open( STDERR, '>&_STDERR' );

    close(RE_OUT) if ($close_output);
    close(RE_ERR) if ($close_error);

    # Capture the output.
    my $stdout = '';
    my $READ_OUT;
    if ( open( $READ_OUT, "<", $stdout_file ) ) {
        $stdout = join( "", <$READ_OUT> );
        close($READ_OUT);
    }
    else {
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT OPEN STDOUT CAPTURE FILE \"$stdout_file\" FOR READING:\n$!");
        return exists $args->{fail_cb}? $args->{fail_cb}->(
                err=> $err,
                cmd=>$cmd, 
                description=>$args->{description}, 
                rc=>$rc, 
                stdin=>$input, 
                stdout=>$stdout, 
            ):0;
    }

    my $stderr = '';
    my $READ_ERR;
    if ( open( $READ_ERR, "<", $stderr_file ) ) {
        $stderr = join( "", <$READ_ERR> );
        close($READ_ERR);
    }
    else {
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT OPEN STDERR CAPTURE FILE \"$stdout_file\" FOR READING:\n$!");
        return exists $args->{fail_cb}? $args->{fail_cb}->(
                err=> $err,
                cmd=>$cmd, 
                description=>$args->{description}, 
                rc=>$rc, 
                stdin=>$input, 
                stdout=>$stdout, 
                stderr=>$stderr 
            ):0;
    }

    $input = '';
    if ($input_file) {
        my $READ_IN;
        if ( open( $READ_IN, "<", $input_file ) ) {
            $input = join( "", <$READ_IN> );
            close($READ_IN);
        }
        else {
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT OPEN STDIN CAPTURE FILE \"$stdout_file\" FOR READING:\n$!");
            return exists $args->{fail_cb}? $args->{fail_cb}->(
                err=> $err,
                cmd=>$cmd, 
                description=>$args->{description}, 
                rc=>$rc, 
                stdin=>$input, 
                stdout=>$stdout, 
                stderr=>$stderr 
            ):0;
        }
    }

    flush_msg(msg=>"\n");
    #Remove temp files.
    unlink($stdout_file);
    unlink($stderr_file);
    unlink($input_file) if ($delete_input_file);

    # Part two of command output, do after output capture is complete.
    output(
        "  Return Code: ${rc}\n"
            . "  Standard Output:\n\t"
            . join( "\n\t", split( /\n(?:\r?)/, $stdout ) ) . "\n"
            . "  Standard Error:\n\t"
            . join( "\n\t", split( /\n(?:\r?)/, $stderr ) ) . "\n",
        'only' => 'logfile'
    );
    

    if($premature_exit){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COMMAND TIMED OUT");
        return exists $args->{fail_cb}? $args->{fail_cb}->( 
                                            err=>$err,
                                            cmd=>$cmd, 
                                            description=>$args->{description}, 
                                            rc=>$rc, 
                                            stdin=>$input, 
                                            stdout=>$stdout, 
                                            stderr=>$stderr 
                                            ) : 0;        
    }

    if($premature_exit){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "TIMEOUT REACHED");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(
            err=>$err,
            cmd=>$cmd, 
            description=>$args->{description}, 
            rc=>($rc), 
            stdin=>$input, 
            stdout=>$stdout, 
            stderr=>$stderr 
        ):0);
    }

    if ($rc == -1) {
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COMMAND FAILED TO EXECUTE");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(
                err=>$err,
                cmd=>$cmd, 
                description=>$args->{description}, 
                rc=>($rc), 
                stdin=>$input, 
                stdout=>$stdout, 
                stderr=>$stderr 
            )
        :0);
    }
    elsif ($rc & 127) {
        if(exists $args->{ignore_codes}){
            foreach my $ignore_code ( @{$args->{ignore_codes} }){
 
                if($ignore_code == $rc){
                    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "CHILD DIED WITH A SIGNAL ".($rc & 127)." ".(($rc & 128)?'WITH':'WITHOUT')." COREDUMP");
                    return (
                        exists $args->{succ_cb}?
                            $args->{succ_cb}->(
                                output=>$output,
                                cmd=>$cmd, 
                                description=>$args->{description}, 
                                rc=>($rc & 127), 
                                stdin=>$input, 
                                stdout=>$stdout, 
                                stderr=>$stderr 
                            )
                            :1
                    );
                }
            }
        }
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "CHILD DIED WITH A SIGNAL ".($rc & 127)." ".(($rc & 128)?'WITH':'WITHOUT')." COREDUMP");
        return (
            exists $args->{fail_cb}?
                $args->{fail_cb}->(
                    err=>$err,
                    cmd=>$cmd, 
                    description=>$args->{description}, 
                    rc=>($rc & 127), 
                    stdin=>$input, 
                    stdout=>$stdout, 
                    stderr=>$stderr 
                )
                :0
        );
    }
    else {
        my $ignore = 0;
        if(exists $args->{ignore_codes}){
            foreach my $ignore_code ( @{$args->{ignore_codes} }){
                if($ignore_code == $rc){
                    $ignore = 1;
                    last;
                }
            }
        }

        if(($rc >> 8)!=0 && !$ignore)
        {
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COMMAND FAILED WITH CODE \"".($rc >> 8)."\"");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(
                err=>$err,
                cmd=>$cmd, 
                description=>$args->{description}, 
                rc=>($rc >> 8), 
                stdin=>$input, 
                stdout=>$stdout, 
                stderr=>$stderr 
                )
            :0
            );
        }else{
            my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "COMMAND COMPLETED");
            return (exists $args->{succ_cb}?$args->{succ_cb}->(
                output=>$output,
                cmd=>$cmd, 
                description=>$args->{description}, 
                rc=>($rc >> 8), 
                stdin=>$input, 
                stdout=>$stdout, 
                stderr=>$stderr 
                )
            :1);
        }
    }

}

#####################################################################################################################################
#---------------------------------------Environment Manipulation Functions----------------------------------------------------------#
#####################################################################################################################################

sub push_to_system_var{
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["name","value"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: push_to_system_var(
            name=>\"EXAMPLE_VAR\",
            value=>\"some_new_value\",
    );
|
    );

    if(!$args){
       return 0;
    }
    my $env_var_value;

    IM_helper::get_reg_value_data(
        key=>'HKEY_LOCAL_MACHINE/System/CurrentControlSet/Control/Session Manager/Environment',
        value=>$args->{name},
        quiet=>1,
        succ_cb=> sub{
            $env_var_value = {@_}->{results};
        }
    );

    my @env_vars = ($env_var_value ? split(';', $env_var_value) : ());

    foreach my $env_var(@env_vars){
        if($env_var eq $args->{value}){
            my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "\"$args->{value}\" ALREADY EXISTS IN \"$args->{name}\"");
            return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):0);
        }
    }
    push(@env_vars,$args->{value});

    # Not sure why setx is required... Doesn't seem to be refreshing without it.
    if(
        !IM_helper::run_command(
                command=>"setx /m $args->{name} ".join(';',@env_vars),
                quiet=>1
            )
        ){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT SET ENVIRONMENT VARIABLE FOR \"$args->{name}\" USING SETX");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0); 
    }

    if(
        IM_helper::set_reg_key_values(
            key=>'HKEY_LOCAL_MACHINE/System/CurrentControlSet/Control/Session Manager/Environment',
            values=>[$args->{name},[join(';',@env_vars),'REG_SZ']],
            quiet=>1
        )
    ){
        my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "ADDED \"$args->{value}\" TO ENVIRONMENT VARIABLE \"$args->{name}\"");
        return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):0);
    }else{
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT STORE ENVIRONMENT VARIABLE FOR \"$args->{name}\"");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);        
    }
}

#####################################################################################################################################
#-------------------------------------------Functions for Manipulating GUIs---------------------------------------------------------#
#####################################################################################################################################

sub get_windows_with_text{
    use Win32::GuiTest;
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["text"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: get_windows_with_text(
        text=>'some sub string of all text',
        succ_cb=>sub{
            my \@window_ids = \@{{\@_}->{results}};
        }
    );

NOTE: This function literally just finds all the windows containing the text passed as the argument
      It then passes the array ref containing all the window IDs to the succ_cb|
    );

    if(!$args){
       return 0;
    }    
    my @results = ();
    foreach my $window (Win32::GuiTest::GetChildWindows(Win32::GuiTest::GetDesktopWindow())){
        my $text = Win32::GuiTest::GetWindowText($window);
        if ($text =~ /$args->{text}/){
            push(@results,$window);
        }
    }
    if(scalar @results){
        my $output = ((exists $args->{quiet} && $args->{quiet})? undef : ("FOUND ".(scalar @results)." WINDOWS WITH TEXT MATCHING \"$args->{text}\"") );
        return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output,results=>\@results):1);
    }else{
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT FIND ANY WINDOWS WITH TEXT MATCHING \"$args->{text}\"");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);            
    }
}

sub close_window{
    use Win32::GuiTest;
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["window"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: close_window(
        window=>115566
    );|
    );

    if(!$args){
       return 0;
    }    
    if(Win32::GuiTest::SendMessage($args->{window}, 0x0010, 0, 0)){
        my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "CLOSED WINDOW \"$args->{window}\"" );
        return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);
    }else{
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT CLOSE WINDOW \"$args->{window}\"");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);            
    }
}

sub send_keys_to_window{
    use Win32::GuiTest;
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["window","keys"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: send_keys_to_window(
        window=>115566,
        keys=>"{TAB}{BS}{DOWN}"
    );

NOTE: This is simply a wrapper for the SendKeys() subroutine in the Win32::Test module

MODULE DOCUMENTATION:

    Sends keystrokes to the active window as if typed at the keyboard using the
    optional delay between key-up and key-down messages (default is 25 ms and
    should be OK for most uses). 

    The keystrokes to send are specified in KEYS. There are several
    characters that have special meaning. This allows sending control codes 
    and modifiers:

        ~ means ENTER
        + means SHIFT 
        ^ means CTRL 
        % means ALT

    The parens allow character grouping. You may group several characters, so
    that a specific keyboard modifier applies to all of them. Groups can
    be enclosed in groups.

    E.g. SendKeys("ABC") is equivalent to SendKeys("+(abc)")

    The curly braces are used to quote special characters (SendKeys("{+}{{}")
    sends a '+' and a '{'). You can also use them to specify certain named actions:

        Name          Action

        {BACKSPACE}   Backspace
        {BS}          Backspace
        {BKSP}        Backspace
        {BREAK}       Break
        {CAPS}        Caps Lock
        {DELETE}      Delete
        {DOWN}        Down arrow
        {END}         End
        {ENTER}       Enter (same as ~)
        {ESCAPE}      Escape
        {HELP}        Help key
        {HOME}        Home
        {INSERT}      Insert
        {LEFT}        Left arrow
        {NUMLOCK}     Num lock
        {PGDN}        Page down
        {PGUP}        Page up
        {PRTSCR}      Print screen
        {RIGHT}       Right arrow
        {SCROLL}      Scroll lock
        {TAB}         Tab
        {UP}          Up arrow
        {PAUSE}       Pause
        {F1}          Function Key 1
        ...           ...
        {F24}         Function Key 24
        {SPC}         Spacebar
        {SPACE}       Spacebar
        {SPACEBAR}    Spacebar
        {LWI}         Left Windows Key
        {RWI}         Right Windows Key 
        {APP}         Open Context Menu Key

    or supply a number that will be treated as a VK code. Note that a single-digit
    number will be treated as a character, so prepend these with '0'.

    All these named actions take an optional integer argument, like in {RIGHT 5}. 
    For all of them, except PAUSE, the argument means a repeat count. For PAUSE
    it means the number of milliseconds SendKeys should pause before proceding.

    In this implementation, SendKeys always returns after sending the keystrokes.
    There is no way to tell if an application has processed those keys when the
    function returns.

    Unicode characters in C<\$keys> are translated into set of ALT+NUMPAD keystrokes.
    Note that not all applications can understand unicode input.|
    );

    if(!$args){
       return 0;
    }    

    if(!Win32::GuiTest::SetForegroundWindow($args->{window})){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT SET WINDOW \"$args->{window}\" TO FOREGROUND");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }
    Win32::GuiTest::SendKeys($args->{keys});

    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "SENT KEYS \"$args->{keys}\" TO WINDOW \"$args->{window}\"" );
    return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);
}

#####################################################################################################################################
#----------------------------------Functions Using Programs and Features Registry Functionality-------------------------------------#
#####################################################################################################################################

sub get_uninstall_strings{
     my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["display_name"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: get_install_locations(
        display_name=>\"Example Program\"
    );

Note: - display_name should be a \"Name\" from the \"Program and Features\"" list|
    );

    if(!$args){
       return 0;
    }

    local $/ = '';

    my %uninstall_commands;

    $uninstall_commands{x64} = `powershell \"Get-ItemProperty HKLM:\\Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\* | Where-Object {\$_.DisplayName -eq \\\"$args->{display_name}\\\"} | Select-Object UninstallString | Format-List
\"`;
     $uninstall_commands{x86} = `powershell \"Get-ItemProperty HKLM:\\Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\* | Where-Object {\$_.DisplayName -eq \\\"$args->{display_name}\\\"} | Select-Object UninstallString | Format-List
\"`;
    my %results_uninstall_strings;
    $results_uninstall_strings{x86} = ();
    $results_uninstall_strings{x64} = ();

    while(my($key,$value) = each %uninstall_commands){
        my @uninstall_strings = split/\n\n/, $value;
        @uninstall_strings = grep /\S/, @uninstall_strings;
        foreach my $i(@uninstall_strings){
            $i =~ s/UninstallString : //;
            $i =~ s/^                  //gm;
            $i =~ s/\R//g;
            $i =~ s/ +/ /;
            push(@{$results_uninstall_strings{$key}},$i);
        }
    }

    if(!(scalar $results_uninstall_strings{x86} || scalar $results_uninstall_strings{x64})){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT FIND \"$args->{display_name}\" UNINSTALL STRING");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }

    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "FOUND UNINSTALL STRINGS FOR \"$args->{display_name}\"");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(
            x86=>$results_uninstall_strings{x86},
            x64=>$results_uninstall_strings{x64},
            output=>$output
        )
    :1); 
}

sub get_install_locations{
     my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["display_name"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: get_install_locations(
        display_name=>\"Example Program\"
    );

Note: - display_name should be a \"Name\" from the \"Program and Features\"" list|
    );

    if(!$args){
       return 0;
    }

    local $/ = '';

    my %uninstall_commands;
    $uninstall_commands{x86} = `powershell \"Get-ItemProperty HKLM:\\Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\* | Where-Object {\$_.DisplayName -eq \\\"$args->{display_name}\\\"} | Select-Object InstallLocation | Format-List
\"`;
     $uninstall_commands{x64} = `powershell \"Get-ItemProperty HKLM:\\Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\* | Where-Object {\$_.DisplayName -eq \\\"$args->{display_name}\\\"} | Select-Object InstallLocation | Format-List
\"`;
    my %results_uninstall_strings;
    $results_uninstall_strings{x86} = ();
    $results_uninstall_strings{x64} = ();

    while(my($key,$value) = each %uninstall_commands){
        my @uninstall_strings = split/\n\n/, $value;
        @uninstall_strings = grep /\S/, @uninstall_strings;
        foreach my $i(@uninstall_strings){
            $i =~ s/InstallLocation : //;
            $i =~ s/^                  //gm;
            $i =~ s/\R//g;
            $i =~ s/ +/ /;
            push(@{$results_uninstall_strings{$key}},$i);
        }
    }

    if(!(scalar $results_uninstall_strings{x86} || scalar $results_uninstall_strings{x64})){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT FIND \"$args->{display_name}\" INSTALL STRING");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }

    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "FOUND INSTALL STRINGS FOR \"$args->{display_name}\"");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(
            x86=>$results_uninstall_strings{x86},
            x64=>$results_uninstall_strings{x64},
            output=>$output
        )
    :1);
}

sub uninstall_using_programs_and_features{
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["display_name"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: uninstall_using_programs_and_features(
        display_name=>'example_display_name'
    );

NOTE: The display_name should be the name that is displayed in the programs and features list|
    );

    if(!$args){
       return 0;
    }

    if(
        IM_helper::get_uninstall_strings(
            display_name=>$args->{display_name},
            succ_cb=> sub{
                my $succ = 0;

                my $uninst_str_x64 = @{{@_}->{x64}}[0];
                if(defined $uninst_str_x64){
                  #Check if uninstall string uses /I (install) switch so replace it with /X (uninstall)
                  $uninst_str_x64=~ s/\Q\/I\E/\/X/;
                  $succ |= IM_helper::run_command(
                    description=>"$args->{display_name} Uninstaller x64",
                    command=>$uninst_str_x64." /quiet /qn",
                    quiet=>((exists $args->{quiet} && $args->{quiet}) ? 1 : 0)
                    );
                }
                my $uninst_str_x86 = @{{@_}->{x86}}[0];
                if(defined $uninst_str_x86){
                  #Check if uninstall string uses /I (install) switch so replace it with /X (uninstall)
                  $uninst_str_x86=~ s/\Q\/I\E/\/X/;
                  $succ |= IM_helper::run_command(
                    description=>"$args->{display_name} Uninstaller x86",
                    command=>$uninst_str_x86." /quiet /qn",
                    quiet=>((exists $args->{quiet} && $args->{quiet}) ? 1 : 0)
                    );
                }

                return $succ;
            }
        )
    ){
        my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "UNINSTALLED \"$args->{display_name}\" USING PROGRAMS AND FEATURES");
        return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);
    }
    else{
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT UNINSTALL \"$args->{display_name}\" USING PROGRAMS AND FEATURES");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);        
    }
}

#####################################################################################################################################
#---------------------------------------------------------Functions For File IO-----------------------------------------------------#
#####################################################################################################################################

use File::Path "remove_tree";

sub set_perms{
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["user","permissions"],
        optional_args => ["folder","file","succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: set_perms(
            user=>\"Users\",
            file=>\"C:\\Example_file.txt\",
            permissions=>"F"
    );

Valid Permissions:
"F"  -> Full
"M"  -> Modify
"RX" -> Read and Execute
"R"  -> Read Only
"W"  -> Write Only|
    );

    if(!$args){
       return 0;
    }

    if(!(!(exists $args->{file}) != !(exists $args->{folder}))){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "EXACTLY ONE OF ARGUMENTS \"file\" OR \"folder\" IS REQUIRED IN FUNCTION".(caller(0))[3]);
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }   

    if(
        !run_command(
            description=>"Setting permissions",
            command=>"icacls \"".( $args->{folder} || $args->{file} )."\" /grant $args->{user}:".(exists $args->{folder} ? "(OI)(CI)" : "")."$args->{permissions} /T",
            fail_cb=> \&IM_helper::catch_error
        )
    ){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "FAILED TO SET PERMISSIONS");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }
    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "SET PERMISSIONS");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);
}

sub rm_dir{
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["dir"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: rm_dir(
        dir=>\"C:\\example_dir\"
    );
|
    );

    if(!$args){
       return 0;
    }

    if(exists $args->{dir}){
        if(!(-e $args->{dir})){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "\"$args->{dir}\" DOESN'T EXIST");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }
        if(!(-d $args->{dir})){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "\"$args->{dir}\" IS NOT A DIRECTORY");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }

        if(!remove_tree($args->{dir}, {error => \my $rm_err})){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT REMOVE DIRECTORY AT \"$args->{dir}\"\n$rm_err");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }
        my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "REMOVED DIRECTORY AT \"$args->{dir}\"");
        return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);
    }    
}


sub rm_dirs{
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["dirs"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: rm_dirs(
        dirs=>[
            \"C:\\example_dir1\",
            \"C:\\example_dir2\"
        ]
    );
|
    );

    if(!$args){
       return 0;
    }

    foreach my $dir (@{$args->{dirs}}){
        #TODO: add fail_cb if not exists
        rm_dir(dir=>$dir);
    }

    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "REMOVED DIRECTORIES");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);
}


sub extract_files {

    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["from","to"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: extract_files(
        from=>[\"C:\\example_file.zip\"],
        to=>\"C:\\example_folder\"
    );
|
    );

    if(!$args){
       return 0;
    }


    foreach my $compressed_file (@{$args->{from}}) {
        if(!(-e $compressed_file)){
            if(-d $compressed_file){
                my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "\"$compressed_file\" IS A DIRECTORY");
                return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
            }          
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT FIND FILE AT \"$compressed_file\"");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }
        if (
            !Archive::Extract->new(
                archive => $compressed_file
            )->extract( to => $args->{to} )
          )
        {
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT EXTRACT \"$compressed_file\" TO \"$args->{to}\"");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }      

        if(!unlink($compressed_file)){
            flush_msg(
                msg=>"COULD NOT REMOVE ZIP FILE AT \"$compressed_file\"",
                type=>'error'
            );
        }
    }

    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "EXTRACTED FILES \"".join(',',@{$args->{from}})."\"");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);
}

sub del_files{
     my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["files"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: del_files(
        files=>[
            'C:\\Example_file.txt',
            'C:\\Example_file1.txt'
        ]
    );|
    );

    if(!$args){
       return 0;
    }

    foreach my $filename (@{$args->{files}}){
        if(!(-e $filename)){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT FIND FILE AT \"$filename\"");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }
        if(-d $filename){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "\"$filename\" IS A DIRECTORY");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }
        if(!(unlink $filename)){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT REMOVE \"$filename\" >> $!");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }
       if(-e $filename){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT REMOVE \"$filename\"");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }
    }

    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "DELETED FILES");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);      
}

sub cp_file{
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["from"],
        optional_args => ["to_dir","to_file","succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: cp_file(
        from=>'C:\\from_file',
        to_file=>'C:\\some_dir\\example_file.txt'
    );

Note: - Exactly one of the two optional arguments \"to_file\" or \"to_dir\" are required.|
    );

    if(!$args){
       return 0;
    }

    if(!(!(exists $args->{to_file}) != !(exists $args->{to_dir}))){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "EXACTLY ONE OF ARGUMENTS \"to_file\" OR \"to_dir\" IS REQUIRED IN FUNCTION ".(caller(0))[3]);
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }

    if(exists $args->{to_dir} && !(-e $args->{to_dir})){
        if(!mkpath($args->{to_dir})){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT BUILD DIRECTORY AT \"$args->{to_dir}\" ");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }
        if(!(-d $args->{to_dir})){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "\"$args->{to_dir}\" IS NOT A DIRECTORY");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }
    }elsif(exists $args->{to_file} && (-e $args->{to_file})){
        if(-d $args->{to_file}){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "\"$args->{to_file}\" IS A DIRECTORY");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }
        if(!unlink($args->{to_file})){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT REMOVE FILE AT \"$args->{to_file}\"");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }
    }

    if(!(-e $args->{from})){
        if(-d $args->{from}){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "\"$args->{from}\" IS A DIRECTORY");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }          
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT FIND FILE AT \"$args->{from}\"");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }

    if(!copy($args->{from},$args->{to_dir} || $args->{to_file})){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT COPY FILE FROM \"$args->{from}\" TO \"".($args->{to_dir} || $args->{to_file})."\n".($!?"$!":""));
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }
    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "COPIED FILES FROM \"$args->{from}\" TO \"".($args->{to_dir} || $args->{to_file})."\n");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);    
}

sub write_file {
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["file","data"],
        optional_args => ["permissions","succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: write_file(
        file=>\"C:\\Example_file.txt\",
        data=>normalize_text(
                text=>qq\|
                        This is some example text
                        More example text
                \|
        ),
        permissions=>0755
    );

Note: - \"permissions\" is an optional parameter.|
    );

    if(!$args){
       return 0;
    }

    my $working_dir = dirname($args->{file});
    if(!(-e $working_dir)){
        if(!mkpath($working_dir,{})){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT BUILD DIRECTORY AT \"$working_dir\"");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }
    }

    my $fh;
    if(!open($fh, '>',$args->{file})){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT OPEN FILE AT \"$args->{file}\" FOR WRITING");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }

    if(!(print $fh $args->{data})){
        close($fh);
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "FAILED TO WRITE TEXT FILE AT \"$args->{file}\"");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }

    close($fh);
    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "WROTE FILE TO \"$args->{file}\"");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);
}


#TODO: Improve so it can handle keys with multiple values
sub transform_ini{
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["old_file","new_file","data"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: transform_ini(
        old_file=>'C:\\from_file.ini',
        new_file=>'C:\\to_file.ini',
        data=[
                {
                    section='EXAMPLE_SECTION',
                    key='example_key',
                    new_value='example_value'
                },
                {
                    section='EXAMPLE_SECTION1',
                    key='example_key',
                    new_value='example_value1'
                }
        ]
    );|
    );

    if(!$args){
       return 0;
    }

    if(!open (INI,"<",$args->{old_file})){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULDN'T OPEN INI FILE AT \"$args->{old_file}\"");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }

    if(!open (NEW_INI,">",$args->{new_file})){
        close (INI);
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULDN'T OPEN INI FILE AT \"$args->{new_file}\"");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }

    sub fio_cleanup{
        close (INI);
        close (NEW_INI);  
    }

    my $curr_section;
    my %ini_hash;

    while (<INI>) {
        my $found = 0;
        chomp;
        if (/^\s*\[(\w+)\].*/) {
            $curr_section = $1;
            if(!(exists $ini_hash{$curr_section})){
                $ini_hash{$curr_section} = ();
            }
        }
        elsif (/^\W*(\w+)\s*=\s*([^;]*)/) {
            my $key = $1;
            my $new_value = $2 || "";
            foreach my $update_obj ( @{$args->{data}}){
                if(!(exists $update_obj->{section} && exists $update_obj->{key})){
                    fio_cleanup();
                    my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "\'section\' AND \'key\' MUST BE INCLUDED IN \'data\' OBJECT");
                    return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
                }
                if(($update_obj->{section} eq $curr_section) && ($update_obj->{key} eq $key)){
                    if(!(exists $update_obj->{new_value})){
                        fio_cleanup();
                        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "\'value\' MUST BE INCLUDED IN \'data\' OBJECT");
                        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
                    }
                    print(NEW_INI "$update_obj->{key} = $update_obj->{new_value}\n");
                    $found = 1;
                }
            }
            push(@{$ini_hash{$curr_section}}, {key=> $key,new_value=> $new_value});
        }
        if($found == 0){
            print NEW_INI $_."\n"
        }
    }

    fio_cleanup();
    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "TRANSFORMED INI \"$args->{old_file}\" TO \"$args->{new_file}\"");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);
}

#####################################################################################################################################
#-------------------------------------------Functions for Manipulating the Registry-------------------------------------------------#
#####################################################################################################################################

sub create_reg_key{
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["root","key"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: create_reg_key(
        root=>'HKEY_CURRENT_USER/',
        key=>'example_key/example_subkey/'
    );|
    );

    if(!$args){
       return 0;
    }

    my $root = $Registry->{$args->{root}};
    if(!$root){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT FIND ROOT_KEY AT \"$args->{root}\"");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }

    my $new_key = $root->CreateKey($args->{key});
    if(!$new_key){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT CREATE REGISTRY KEY AT \"$args->{root}$args->{key}\"");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }
    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "CREATED REGISTRY KEY AT \"$args->{root}$args->{key}\"");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);   
}

sub delete_registry_key{
        my $args = parse_args(
            function_name => (caller(0))[3],
            passed_args => {@_},
            required_args => ["key",],
            optional_args => ["succ_cb","fail_cb","quiet"],
            usage => 
    qq|
    EX: delete_registry_key(
            key=>'example_key_path/example_key_subpath/'
        );|
        );

        if(!$args){
           return 0;
        }

        my $key = $args->{key};
        my $delimiter = $Registry->Delimiter;
        $key .= $delimiter unless $key =~ /\Q$delimiter\E$/;  # tack on a delimiter if one wasn't passed
        delete $Registry->{$key};
        my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "DELETED REGISTRY KEY AT \"$args->{key}\"");
        return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1); 
}

sub reg_key_exists{
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["key"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: reg_key_exists(
        key=>'example_key/example_subkey/'
    );|
    );

    if(!$args){
       return 0;
    }

    my $key = $Registry->{$args->{key}};
    if(!$key){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT FIND ROOT_KEY AT \"$args->{key}\"");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }

    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "FOUND REGISTRY KEY AT \"$args->{key}\"");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1); 
}

sub set_reg_key_values{
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["key","values"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: set_reg_key_values(
        key=>'HKEY_CURRENT_USER/example_key/',
        values=> ['example_values'=>['0x00000000','REG_DWORD']]
    );

Note: - Key delimiter is \"/\"
|
    );

    if(!$args){
       return 0;
    }

    my $key = $Registry->{$args->{key}};
    if(!$key){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT FIND KEY AT \"$args->{key}\"");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }
    if(!$key->SetValue(@{$args->{values}})){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT SET VALUES $args->{values} TO $args->{key}");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }

    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "SET REGISTRY AT \"$args->{key}\"");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1); 
}

sub get_reg_value_data{
    my $args = parse_args(
        function_name => (caller(0))[3],
        passed_args => {@_},
        required_args => ["key","value"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        usage => 
qq|
EX: get_reg_value_data(
        key=>'example_key/example_subkey',
        value=>'example_value'
    );|
    );

    if(!$args){
       return 0;
    }

    my $results = $Registry->{"$args->{key}//$args->{value}"};
    if(!$results){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT FIND VALUE AT \"$args->{key}//$args->{value}\"");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
    }

    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "FOUND REGISTRY AT \"$args->{key}\"");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(results=>$results,output=>$output):1); 
}

#####################################################################################################################################
#------------------------------------Functions for Manipulating ActiveSetup Actions-------------------------------------------------#
#####################################################################################################################################

sub create_activesetup{
    my $args = parse_args(
        function_name =>(caller(0))[3],
        required_args => ["command"],
        optional_args => ["description","version","succ_cb","fail_cb","quiet"],
        passed_args => {@_},
        usage => 
qq|
EX: create_activesetup(
        command=>'example_cmd.exe example_args',
        description=>'This is an example description',
        version=>'1.1'
    );

Note: - The arguments 'version' and 'description' are both optional and will be defaulted to '1.0' and '' respectively|
    );

    if(!$args){
       return 0;
    }

    if (
        !create_activesetup_action(
            'Identifier'  => get_package_id(),
            'ComponentID' => get_package_id(),
            'Description' => $args->{description} || '',
            'Version'     => $args->{version} || '1.0',
            'StubPath'    => $args->{command}
        )
      )
    {
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>"COULD NOT CREATE ACTIVE SETUP TASK"):0);
    }

    chomp(my $username = `whoami`);
    if ( !( $username eq "nt authority\\system" ) ) {
        flush_msg(
            msg=>": DETECTED USER IS NOT SYSTEM ACCOUNT\n"
        );
        if (!run_command(
                description=>"ActiveSetup action as \"$username\"",
                command=>$args->{command},
                show_output=>1
            )
            ){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT RUN ACTIVE SETUP ACTION \"$args->{command}\"");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);
        }
    }
    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "ACTIVE SETUP ACTION CREATED");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1); 
}

sub delete_activesetup {
    my $args = parse_args(
        function_name =>(caller(0))[3],
        required_args => ["id"],
        optional_args => ["succ_cb","fail_cb","quiet"],
        passed_args => {@_},
        usage => 
qq|
EX: delete_activesetup_action(
        id=>'example_identifier.1_0'
    );

Note: - "id" is optional and will default to the packages id|
    );

    if(!$args){
       return 0;
    }
    my $activesetup_root = 'HKEY_LOCAL_MACHINE/SOFTWARE/Microsoft/Active Setup/Installed Components';
    my $activesetup_key = (exists $args->{id} ? $args->{id} : get_package_id());
    
    delete_registry_key(
       key=>"LMachine/SOFTWARE/Microsoft/Active Setup/Installed Components/$activesetup_key"
    );

    if(reg_key_exists(key=>"$activesetup_root/$activesetup_key/",quiet=>1)){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT DELETE ACTIVESETUP ACTION");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);        
    }else{
        my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "DELETED ACTIVESETUP ACTION");
        return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);            
    }
}

#####################################################################################################################################
#------------------------------------Functions for Implementing Socket Functionality------------------------------------------------#
#####################################################################################################################################

use Socket;

my $OUTPUT_SOCKET = undef;

sub socket_send{
	if(defined $OUTPUT_SOCKET){
		return print $OUTPUT_SOCKET shift;
	}else{
		return 0;
	}
}

sub init_socket_conn{
    if($IM_HELPER_OPTIONS{socket_server}){
        my $args = parse_args(
            function_name =>(caller(0))[3],
            required_args => [],
            optional_args => ["succ_cb","fail_cb","quiet"],
            passed_args => {@_},
            usage => 
qq|
    EX: init_socket_conn();|
        );

        if(!$args){
           return 0;
        }

        my ($ip,$port) = ($IM_HELPER_OPTIONS{socket_server} =~ /([0-9]+(?:\.[0-9]{0,3}){3}):([0-9]+)?/);
        if(!$ip || !$port){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "INVALID IP OR PORT FOR SOCKET SERVER");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);   
        }

        my $paddr = sockaddr_in($port, inet_aton($ip));

        if(!socket($OUTPUT_SOCKET,PF_INET,SOCK_STREAM,(getprotobyname('tcp'))[2])){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT CREATE SOCKET");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0);  
        }

        if(!connect($OUTPUT_SOCKET, $paddr)){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT CONNECT TO SOCKET WITH IP \"$ip\" USING PORT \"$port\"");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0); 
        }

        $OUTPUT_SOCKET->autoflush(1);

        flush_msg(
            msg=>"\n"
        );

        socket_send(
		    "\$INIT_TIME={{".localtime->datetime(T => q{ })."}}".
		    "\$HOSTNAME={{".hostname."}}".
		    "\$PACKAGE_ID={{".$INSTALLMONKEY_OPTIONS->{package_id}."}}".
		    "\$STATUS={{warning}}".
		    "\$PROGRESS={{0}}"
        );

        push(@OUTPUT_STREAM,\&socket_send);

        my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "SOCKET CONNECTION ESTABLISHED");
        return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);    
    }
    return 1;
}

sub cleanup_socket{

    my $args = parse_args(
        function_name =>(caller(0))[3],
        required_args => [],
        optional_args => ["succ_cb","fail_cb","quiet"],
        passed_args => {@_},
        usage => 
qq|
EX: cleanup_socket();|
    );

    if(!$args){
       return 0;
    }

    if($OUTPUT_SOCKET){
        if(!close($OUTPUT_SOCKET)){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "COULD NOT CLOSE SOCKET");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0); 
        }
    }

    my $output = ((exists $args->{quiet} && $args->{quiet})? undef : "SOCKET CLOSED");
    return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>$output):1);
}

#####################################################################################################################################
#------------------------------------------------Functions for Handling Output------------------------------------------------------#
#####################################################################################################################################

BEGIN {

    @OUTPUT_STREAM = (
        \&output
    );

    ### Default Exception Handlers ###

    sub catch_success{
        my %args = @_;
        if(%args && exists $args{output} && defined $args{output}){
            flush_msg(
                msg=>$args{output},
                type=>'success'
            );
        }
        return 1;
    }

    sub catch_error{
        my %args = @_;
        if(%args && exists $args{err} && defined $args{err}){
            flush_msg(
                msg=>$args{err},
                type=>'error'
            );
        }
        return 0;
    }

    ### Exception Handler Transformations ###

    %OUTPUT_TRANSFORMATIONS = {};

    $OUTPUT_TRANSFORMATIONS{title} = sub {

        my ($width, $height) = $CONSOLE->Size();
        my $msg = shift;

        my $output = "";

        my $max_line_length = $width - 1;

        if(!length($msg)){
            $output .= ("="x$max_line_length)."\n";
        }
        else{
            my @lines = unpack("(A$max_line_length)*", $msg);

            foreach my $line (@lines){
                my $spaces_before_float = ($max_line_length/2) - (length($line)/2);
                my $spaces_before = int($spaces_before_float + $spaces_before_float/abs($spaces_before_float*2 || 1));
                my $spaces_after = $max_line_length - length($line) - $spaces_before;
                $output .= ("="x$spaces_before).($line).("="x$spaces_after)."\n";

            }
        }

        return $output;
    };

    $OUTPUT_TRANSFORMATIONS{error} = sub {
        my $msg = shift;
        return (":: ERROR >> $msg\n\n");
    };
    $OUTPUT_TRANSFORMATIONS{running} = sub {
        my $msg = shift;
        return (": RUNNING >> $msg\n");
    };
    $OUTPUT_TRANSFORMATIONS{success} = sub {
        my $msg = shift;
        return (": SUCCESS >> $msg\n\n");
    };
    $OUTPUT_TRANSFORMATIONS{done} = sub {
        my $msg = shift;
        return (": DONE\n\n");
    };
    $OUTPUT_TRANSFORMATIONS{default} = sub {
        my $msg = shift;
        return ("$msg");
    };
    $OUTPUT_TRANSFORMATIONS{box_light} = sub {
 
        my ($width, $height) = $CONSOLE->Size();
        my $msg = shift;
 
        my $output = "";
        my $max_line_length = $width - 3;
        my @lines = unpack("(A$max_line_length)*", $msg);
 
        $output .= "-".("-"x($width-3))."-\n";
 
        foreach my $line (@lines){
            my $spaces_before_float = ($max_line_length/2) - (length($line)/2);
            my $spaces_before = int($spaces_before_float + $spaces_before_float/abs($spaces_before_float*2 || 1));
            my $spaces_after = $max_line_length - length($line) - $spaces_before;
            $output .= "|".(" "x$spaces_before).($line).(" "x$spaces_after)."|\n";
         }
        
        $output .= "-".("-"x($width-3))."-\n";
        return $output;
    };
} 

sub flush_msg{
    my $args = parse_args(
            function_name =>(caller(0))[3],
            required_args => ["msg"],
            optional_args => ["type","succ_cb","fail_cb"],
            passed_args => {@_},
            usage => 
qq|
EX: flush_msg(
    msg=>"Printing Hello World",
    type=>'success'
);

Note: - type is defaulted to \"default\"|
        );

    if(!$args){
       return 0;
    }

    if(!scalar @OUTPUT_STREAM){
        my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "OUTPUT STREAM EMPTY");
        return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0); 
    }

    if(exists $args->{type} && $args->{type}){
        if(!(exists $OUTPUT_TRANSFORMATIONS{$args->{type}})){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "OUTPUT TRANSFORMATION \"$args->{type}\" DOES NOT EXIST");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0); 
        }
        if(!(ref $OUTPUT_TRANSFORMATIONS{$args->{type}} eq "CODE")){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "OUTPUT TRANSFORMATION \"$args->{type}\" IS NOT A SUBROUTINE");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0); 
        }
    }else{
        $args->{type} = 'default';
        if(!(exists $OUTPUT_TRANSFORMATIONS{$args->{type}})){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "OUTPUT TRANSFORMATION \"$args->{type}\" DOES NOT EXIST");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0); 
        }
        if(!(ref $OUTPUT_TRANSFORMATIONS{$args->{type}} eq "CODE")){
            my $err = ((exists $args->{quiet} && $args->{quiet})? undef : "OUTPUT TRANSFORMATION \"$args->{type}\" IS NOT A SUBROUTINE");
            return (exists $args->{fail_cb}?$args->{fail_cb}->(err=>$err):0); 
        }
    }
    my $msg = $args->{msg};
    foreach my $output_func(@OUTPUT_STREAM){
        $output_func->($OUTPUT_TRANSFORMATIONS{$args->{type}}->($args->{msg}));
    }

    return (exists $args->{succ_cb}?$args->{succ_cb}->(output=>undef):1); 
}

1;