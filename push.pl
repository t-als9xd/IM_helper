use strict;
use warnings;

use File::Copy;
use Getopt::Long;
use JSON "decode_json";

my %ARGS;

my $CONFIG_FILE = 'config.json';

my @REQUIRED_ARGS = (
	'packages',
	'im_helper_location'
);

GetOptions
(
    'help' => \$ARGS{help},
    'packages=s' => \$ARGS{packages}
);

if($ARGS{packages}){
	my @split = split(',',$ARGS{packages});
	$ARGS{packages} = \@split;
}

local $/ = undef;
open FILE, $CONFIG_FILE or die "Couldn't open config file: $!";
binmode FILE;
my $json_text = <FILE>;
close FILE;

my $JSON_ARGS = decode_json($json_text);
foreach my $arg (@REQUIRED_ARGS){
	if(!(defined $ARGS{$arg})){
		if($JSON_ARGS->{$arg}){
			$ARGS{$arg} = $JSON_ARGS->{$arg};
		}else{
			die("ERROR: ARG \"$arg\" REQUIRED");
		}

	}
}

my $module = $ARGS{im_helper_location};
foreach my $package (@{$ARGS{packages}}){
	my $package_path = "\\\\minerfiles.mst.edu\\dfs\\software\\itwindist\\win10\\appdist\\$package";
	print "Copying \"$module\" to \"$package_path\"\n";
	if(copy(
		$module,
		$package_path
	)){
		print "Success\n";
	}else{
		die "Failed";
	}
}
exit(0);
