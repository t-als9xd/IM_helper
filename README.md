# Install Monkey Helper

[Perl Module](#perl-module)

- [How to Import](#how-to-import)

- [Using Callstacks](#using-callstacks)

- [Passing Arguments to IM_helper Subroutines](#passing-arguments-to-im_helper-subroutines)

- [Real World Example](#real-world-example)

[Management Interface](#management-interface)

[Bulk Package Testing Utility](#bulk-package-testing-utility)

## Perl Module

### How to Import
In order to use IM_helper you need to import it into your perl package. To do this, first make sure IM_helper.pm is in same directory as update.pl. Then you can import it using the following code.

```perl
use IM_helper(
    \%INSTALLMONKEY_OPTIONS,
    v4.04
);
```

### Using Callstacks

IM_helper's import function also has a 3rd optional argument which is a list/set of [callstacks](https://en.wikipedia.org/wiki/Call_stack). Each callstack contains an array of subroutines. When a callstack is executed each of it's subroutines are run in order. If a subroutine fails (returns false), the callstack with exit and fail.

A typical callstack set might look like this.

```perl 
my @call_stack_set = (
    "EXAMPLE_FAIL"=>[
        sub{
            return 1;
        },
        sub{
            return 0;
        }
    ],
    "EXAMPLE_SUCCESS"=>[
        sub{
            return 1;
        }
    ]
);

#You can then import like this
use IM_helper(
    \%INSTALLMONKEY_OPTIONS,
    v4.04,
    \@call_stack_set
);
```

IM_helper uses callstacks for a number of reasons. 
The main reasons are as follows:
- Check for functionality
    
    For example by using ```update.pl --list``` you can get a list of all the available callstacks. You can then use that out to check if uninstall functionality has been implemented within the package. If you want to use only certain callstacks you can run ```update.pl --override DO_THIS,THEN_THAT```.

- Progress tracking

    For example you can count the number of subroutines within some callstack and use that information to track its progress. This is how the Management Interface is able to display a progress bar.
- Simplified code.

```perl
# Instead of this
if(!run_command("echo hello")){
    return(0);
}
# You can use this because perl subroutines implicitly return the last value in itself (in this case the return value of run_command)
sub{run_command("echo hello")}
```
- Better Organization of Conditionals 

    The following code would allow a user to license by calling ```perl update.pl``` or just check how it is licensed using ```perl update.pl --override
    CHECK_LICENSING```

```perl

sub License_using_A{
    # Do bunch of A specific stuff
    return 1;
}

sub License_using_B{
    # Do bunch of B specific stuff
    return 1;
}

use IM_helper(
    \%INSTALLMONKEY_OPTIONS,
    v4.04,
    (
        "CHECK_LICENSING" => [
            sub {
                if(-e "C:\Licensing_A.exe"){
                    push($IM_helper::call_stack_set{LICENSE},\&License_using_A);
                    system("echo A");
                }else{
                    push($IM_helper::call_stack_set{LICENSE},\&License_using_B);
                    system("echo B");
                }
            }
        ],
        "LICENSE" => [
            sub{
                copy("C:\\License_file.txt","C:\\Some_dir\\License_file.txt");
            }
        ]
    )
);

if(defined $IM_helper::exec_call_stack_override){
    $IM_helper::exec_call_stack_override->(); # Run callstacks passed to update.pl under --override switch
}
else{
    IM_helper::execute_call_stacks(ids=>["CHECK_FOR_LICENSING_IMPLEMENTATION","LICENSE"]);
}
```

### Passing Arguments to IM_helper Subroutines

For almost every subroutine within IM_helper, arguments are passed as hash refs. 

```perl 
IM_helper::run_command(
    command=>'echo "Hello World!"'
);
```
The reason for this is IM_helper checks internally that

- all required arguments are passed
- all arguments passed are either required or optional
    
If either of those conditions are not met, IM_helper will try to guess what you meant to pass.

Additionaly almost every subroutine within IM_helper also has these optional arguments:

- quiet (disable IM_helper output)
- succ_cb
- fail_cb

Here's how to use them:

```perl 
IM_helper::run_command(
    command=>'echo "Hello World!"',
    quiet=>1,
    succ_cb=>sub{
        print "Success!\n";
        return 1;
    },
    fail_cb=>sub{
        print "Failed\n"
        return 0;
    }
);
```

### Real World Example

```perl
use strict;
use warnings;

our %INSTALLMONKEY_OPTIONS;

BEGIN {
    %INSTALLMONKEY_OPTIONS = (
        package_id       => 'EXAMPLE.2017',
        package_revision => '',
    );
}

use lib ( '\\\\minerfiles.mst.edu\dfs\software\loginscripts\im',
    $ENV{'WINDIR'} . '\SYSTEM32\UMRINST', 'C:\temp' );

use InstallMonkey::Shared;

use IM_helper(
    \%INSTALLMONKEY_OPTIONS,
    v4.04,
    (
        "INSTALL" =>
        [
            sub {
                IM_helper::run_command(
                	command => "install_example_A.exe --quiet"
                );
            },
            sub {
                IM_helper::run_command(
                	command => "install_example_B.exe --quiet"
                );          	
            }
        ],
        "UNINSTALL" => 
        [
            sub {
                IM_helper::uninstall_using_programs_and_features(
                    display_name=> "example 2017 (x64)"
                );
            },
        ]
    )
);

do_install(
    allowed_versions => [],
    allowed_os_architectures => [],
    allowed_regs =>
      [ 'clc', 'desktop', 'traveling', 'virtual-clc', 'virtual-desktop' ],
    no_install_check => IM_helper::override_exists(id=>'UNINSTALL',quiet=>1),
    no_product_key => IM_helper::override_exists(id=>'UNINSTALL',quiet=>1),
    install_sub => $IM_helper::exec_call_stack_override || sub{IM_helper::exec_call_stacks(ids=>["INSTALL"])}
);

IM_Exit(EXIT_SUCCESS);
```

## Management Interface
Watch and log package installations.

### How To Use
1. Install node.js
2. Change directory to IM_helper\server
3. run ```node index.js```

## Bulk Package Testing Utility